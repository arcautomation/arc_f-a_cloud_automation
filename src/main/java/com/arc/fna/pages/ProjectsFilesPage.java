package com.arc.fna.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;

import com.arc.fna.utils.PropertyReader;
import com.arc.fna.utils.SkySiteUtils;
import com.arc.fna.utils.randomFileName;
import com.arc.fna.utils.randomSharedName;
import com.arcautoframe.utils.Log;

public class ProjectsFilesPage extends LoadableComponent<ProjectsFilesPage>
{

	WebDriver driver;
	private boolean isPageLoaded;
	
	FnaHomePage fnaHomePage;
	
	/**
	 * Identifying web elements using FindfBy annotation.
	 */
	@FindBy(xpath = "//*[@id='setting']")
	WebElement btnSetting;

	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnSetting, 60);
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * 
	 * @param driver
	 */
	public ProjectsFilesPage(WebDriver driver) 
	{
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@FindBy(xpath = "(//tbody/tr[2]/td[2]/table/tbody/tr[1]/td[4]/span)[2]")
	WebElement st1folder;
	
	@FindBy(css = "#chkAllProjectdocs")
	WebElement chkAllProjectdocs;	
	
	@FindBy(css = "#Button1")
	WebElement btnButton1;		
	
	@FindBy(css = "#liCopyFile1>a")
	WebElement liCopyFile1;
	
	@FindBy(xpath = "//*[@id='popupTree']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")
	WebElement btnpopupfolder2;
	
	@FindBy(xpath = "//*[@id='popup']/div/div/div[3]/div[2]/input[1]")
	WebElement btnpopupfolder;
	
	/** 
     * Method written for Copy files within collection(New revision)
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean CopyFiles_Within_Collection()
    {
    	boolean result1=false;    	 
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	st1folder.click();
    	Log.message("1st Folder has been clicked");
		SkySiteUtils.waitTill(3000);
		chkAllProjectdocs.click();//click on AllCheckbox image
		Log.message("AllCheck box has been clicked");
		SkySiteUtils.waitTill(3000);
		btnButton1.click();//click on more
		Log.message("more button has been clicked");
		SkySiteUtils.waitTill(3000);
		liCopyFile1.click();//click on copy files
		Log.message("copy files has been clicked");
		SkySiteUtils.waitTill(3000);
		btnpopupfolder2.click();
		Log.message("exp popup folder has been clicked");
		SkySiteUtils.waitTill(3000);
		btnpopupfolder.click();//click on Ok button
		Log.message("Ok button has been clicked");
		SkySiteUtils.waitTill(10000);	
		//driver.findElement(By.xpath("(//table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span)[1]")).click();
		
		driver.findElement(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")).click();//click on 2nd Folder
		SkySiteUtils.waitTill(5000);		
		if(driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
		{
			result1=true;
			Log.message("Files Copied Exp Floder Successed!!!!");
		}
		else
		{
			result1=false;
			Log.message("Files Copied in Exp Floder Failed!!!!");
		}
		if((result1==true))
			return true;
		else
			return false;
    }
    
    @FindBy(css = "#liMoveFile1>a")
	WebElement liMoveFile1;
    
    /** 
     * Method written for Move files within collection
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean MoveFiles_Within_Collection()
    {
    	boolean result1=false;    	 
    	SkySiteUtils.waitTill(5000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	st1folder.click();
    	Log.message("1st Folder has been clicked");
		SkySiteUtils.waitTill(3000);
		chkAllProjectdocs.click();//click on AllCheckbox image
		Log.message("AllCheck box has been clicked");
		SkySiteUtils.waitTill(3000);
		
		String BeforeFile_Count = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div")).getText();// Getting label counts
		Log.message("Before file count is: " + BeforeFile_Count);// Getting Lite user counts
		String[] x = BeforeFile_Count.split(" ");
		String AvlBeforeFile_Count = x[3];
		Log.message(AvlBeforeFile_Count);
		int Before_Count = Integer.parseInt(AvlBeforeFile_Count);
		SkySiteUtils.waitTill(3000);
		btnButton1.click();//click on more
		Log.message("more button has been clicked");
		SkySiteUtils.waitTill(3000);
		liMoveFile1.click();//click on copy files
		Log.message("move files has been clicked");
		SkySiteUtils.waitTill(3000);
		btnpopupfolder2.click();
		Log.message("exp popup folder has been clicked");
		SkySiteUtils.waitTill(3000);
		btnpopupfolder.click();//click on Ok button
		Log.message("Ok button has been clicked");
		SkySiteUtils.waitTill(10000);		
		driver.findElement(By.xpath("//*[@id='treeboxbox_tree_wrap_container']/div/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr/td[4]/span")).click();//click on 2nd Folder
		Log.message("2nd folder has been clicked");
		SkySiteUtils.waitTill(5000);		
		String AfterFile_Count = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div")).getText();// Getting label counts
		Log.message("After file count is: " + AfterFile_Count);// Getting Lite user counts
		String[] y = AfterFile_Count.split(" ");
		String AvlAfterFile_Count = y[3];
		Log.message(AvlAfterFile_Count);
		int After_Count = Integer.parseInt(AvlAfterFile_Count);		
		SkySiteUtils.waitTill(3000);
		if(driver.findElement(By.xpath("//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[2]/a[3]")).isDisplayed())
		{
			result1=true;
			Log.message("Files moved in Exp Floder Successed!!!!");
		}
		else
		{
			result1=false;
			Log.message("Files moved in Exp Floder Failed!!!!");
		}
		if((result1==true)&&(Before_Count==After_Count))
			return true;
		else
			return false;
    }
    
    @FindBy(xpath = "//*[@id='divGrid']/div[2]/table/tbody/tr[2]/td[1]/img")
   	WebElement btncheck;
    
    @FindBy(css = "#liDeleteFile1>a")
   	WebElement liDeleteFile1;
    
    @FindBy(css = "#button-1")
   	WebElement btnbutton1;
    
    
    /** 
     * Method written for Delete file within folder
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean DeleteFile_Within_Folder()
    {
    	   	 
    	SkySiteUtils.waitTill(2000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	btncheck.click();
    	Log.message("check box button has been clicked");
		SkySiteUtils.waitTill(3000);
		String BeforeFile_Count = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div")).getText();// Getting label counts
		Log.message("Before file count is: " + BeforeFile_Count);// Getting Lite user counts
		String[] x = BeforeFile_Count.split(" ");
		String AvlBeforeFile_Count = x[3];
		Log.message(AvlBeforeFile_Count);
		int Before_Count = Integer.parseInt(AvlBeforeFile_Count);
		SkySiteUtils.waitTill(3000);
		btnButton1.click();//click on more
		Log.message("more button has been clicked");
		SkySiteUtils.waitTill(3000);
		liDeleteFile1.click();//click on copy files
		Log.message("delete files has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.switchTo().defaultContent();
		btnbutton1.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(5000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame")));
		String AfterFile_Count = driver.findElement(By.xpath("//*[@id='divPaginationBelow']/div[2]/div/div")).getText();// Getting label counts
		Log.message("After file count is: " + AfterFile_Count);// Getting Lite user counts
		String[] y = AfterFile_Count.split(" ");
		String AvlAfterFile_Count = y[3];
		Log.message(AvlAfterFile_Count);
		int After_Count = Integer.parseInt(AvlAfterFile_Count);		
		SkySiteUtils.waitTill(3000);
		if(After_Count==(Before_Count-1))
			return true;
		else
			return false;
    }
    
    @FindBy(xpath = "(//i[@class='icon icon-link-files'])[3]")
   	WebElement btniconlinkfiles;
    
    @FindBy(css = "#btnAddToCommunication")
   	WebElement btnAddToCommunication;
    
    @FindBy(css = "#liCheckoutFile1>a")
	WebElement liCheckoutFile;
    
    @FindBy(xpath = "//i[@class='icon icon-check-outFiles icon-orange']")
	WebElement Checkouticon;
    
    @FindBy(css = "#Button1")
	WebElement btnmore;
    
    @FindBy(css = "#liUndoPendingChanges1>a")
   	WebElement btnliUndoPendingChanges1;    
    
    @FindBy(xpath = "//span[@class='noty_text']")
	WebElement notytext;
    
    /** 
     * Method written for Undo changes file within folder
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
	
    public boolean Undochanges_Within_Folder() throws AWTException
    {
    	boolean result1=false;  	 
    	SkySiteUtils.waitTill(2000);
    	driver.switchTo().frame(driver.findElement(By.id("myFrame")));
    	btncheck.click();
    	Log.message("check box button has been clicked");
		SkySiteUtils.waitTill(3000);
		btnButton1.click();//click on more
		Log.message("more button has been clicked");
		SkySiteUtils.waitTill(3000);
		liCheckoutFile.click();
		Log.message("Check out files has been clicked");
		SkySiteUtils.waitTill(5000);
		// Handling Download PopUp using robot
		Robot robot = null;
		robot = new Robot();
		SkySiteUtils.waitTill(5000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		SkySiteUtils.waitTill(30000);
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_F4);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_F4);
		SkySiteUtils.waitTill(5000);		
		if (Checkouticon.isDisplayed()) 
		{
			result1=true;
			Log.message("selected file has been Check out successfully");
		}
		else 
		{
			result1=false;
			Log.message("selected file has been Check out Unsuccessfully");			
		}		
		SkySiteUtils.waitTill(3000);
		btnmore.click();
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(1000);
		btnliUndoPendingChanges1.click();
		Log.message("Undo changes has been clicked");
		SkySiteUtils.waitTill(2000);
		driver.switchTo().defaultContent();		
		btnbutton1.click();
		Log.message("Yes button has been clicked");
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, notytext, 60);
		String Notify_msg = notytext.getText();
		Log.message("Notify Message is:" + Notify_msg);
		if((Notify_msg.contains("Undo changes successfully finished."))&&(result1==true))
			return true;
		else
			return false;		
    }
    
    
    @FindBy(css = ".icon.icon-download.icon-orange")
   	WebElement btndownloadiconorange;
    
    @FindBy(css = ".file-name")
   	WebElement txtfilename;
       
    /** 
     * Method written for Undo changes file within folder
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
   	
    public boolean Singlefile_Download_Within_Folder(String DownloadPath) throws AWTException
    {
    	SkySiteUtils.waitTill(2000);      	
       	// Calling delete files from download folder script
       	fnaHomePage = new FnaHomePage(driver).get();
       	fnaHomePage.Delete_Files_From_Folder(DownloadPath);
       	SkySiteUtils.waitTill(3000);       	
       	driver.switchTo().frame(driver.findElement(By.id("myFrame")));  
       	String FileName= txtfilename.getText();
       	Log.message("File name is:"+FileName);
       	SkySiteUtils.waitTill(3000); 
       	btndownloadiconorange.click();
       	Log.message("Download icon button has been clicked");
   		SkySiteUtils.waitTill(15000);   			
   		// Get Browser name on run-time.
   		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   		String browserName = caps.getBrowserName();
   		Log.message("Browser name on run-time is: " + browserName);
   		
   		if (browserName.contains("firefox"))
   		{
   			// Handling Download PopUp of firefox browser using robot
   			Robot robot = null;
   			robot = new Robot();   			
   			robot.keyPress(KeyEvent.VK_ALT);
   			SkySiteUtils.waitTill(2000);
   			robot.keyPress(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(2000);
   			robot.keyRelease(KeyEvent.VK_ALT);
   			robot.keyRelease(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(3000);
   			robot.keyPress(KeyEvent.VK_ENTER);
   			robot.keyRelease(KeyEvent.VK_ENTER);
   			SkySiteUtils.waitTill(20000);
   		}
   		SkySiteUtils.waitTill(10000);
   		// After checking whether download folder or not
   		String ActualFilename = null;
   		
   		File[] files = new File(DownloadPath).listFiles();

   		for (File file : files) 
   		{
   			if (file.isFile())
   			{
   				ActualFilename = file.getName();// Getting Folder Name into a variable
   				Log.message("Actual File name is:" + ActualFilename);
   				SkySiteUtils.waitTill(1000);
   				Long ActualFileSize = file.length();
   				Log.message("Actual File size is:" + ActualFileSize);
   				SkySiteUtils.waitTill(3000);
   				if (ActualFileSize != 0)
   				{
   					Log.message("Downloaded file is available in downloads!!!");
   				}
   				else
   				{
   					Log.message("Downloaded file is NOT available in downloads!!!");
   				}
   			}
   		}
   		if (ActualFilename.contentEquals(FileName))
   			return true;
   		else
   			return false;   		
    }
    
    
    /**
	 * Method written for Copy files different collection with create new revision option
	 * Scripted By: Sekhar
	 * 
	 * @return
	 * @throws AWTException
	 */
	public boolean CopyFiles_different_Collection(String projectName1, String FolderName1) throws AWTException
	{
		boolean result1 = false;
		boolean result2 = false;
		boolean result3 = false;
		SkySiteUtils.waitTill(3000);
		driver.switchTo().frame(driver.findElement(By.id("myFrame"))); 
		driver.findElement(By.xpath("//input[@id='chkAllProjectdocs']")).click();// select check box
		Log.message("all check box has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("(//i[@class='icon icon-option ico-lg'])[3]")).click();// click on More Option
		Log.message("more option has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("(//i[@class='icon icon-copy-files'])[2]")).click();// click on Copy files
		Log.message("copyfiles button has been clicked");
		SkySiteUtils.waitTill(3000);
		driver.findElement(By.xpath("//button[@id='btnProjectListSearch']")).click();// select the project Target
		Log.message("target project has been clicked");
		SkySiteUtils.waitTill(3000);

		int Project_Count = 0;
		int i = 0;
		List<WebElement> allElements = driver.findElements(By.xpath("//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label"));
		for (WebElement Element : allElements)
		{
			Project_Count = Project_Count + 1;
		}
		Log.message("Project Count is: " + Project_Count);
		// Getting Project Names and machine
		for (i = 1; i <= Project_Count; i++) 
		{
			String Exp_ProjectName1 = driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label)[" + i + "]")).getText();
			Log.message("Exp Project Name is: " + Exp_ProjectName1);

			if (Exp_ProjectName1.contains(projectName1))
			{
				result1 = true;
				Log.message("Select Project is Available");
				driver.findElement(By.xpath("(//*[@id='plctrl_divProjectList']/ul/li/a/span[2]/label)[" + i + "]")).click();
				Log.message("Exp project has been clicked");
				SkySiteUtils.waitTill(3000);
				break;
			}
		}
		if (result1 == true)
		{
			Log.message("Exp Project Successfully!!!!");
			int Folders_Count = 0;
			int j = 0;
			List<WebElement> allElements1 = driver.findElements(By.xpath("(//span[@class='standartTreeRow'])"));
			for (WebElement Element : allElements1)
			{
				Folders_Count = Folders_Count + 1;
			}
			Log.message("Folder Count: " + Folders_Count);
			// Getting Folder Names and machine
			for (j = 1; j <= Folders_Count; j++)
			{
				String Exp_FolderName1 = driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[" + j + "]")).getText();
				Log.message("Exp Folder Name: " + Exp_FolderName1);
				if (Exp_FolderName1.contentEquals(FolderName1))
				{
					result2 = true;
					Log.message("Select Folder is Available");
					driver.findElement(By.xpath("(//span[@class='standartTreeRow'])[" + j + "]")).click();
					Log.message("Exp root folder has been clicked");
					break;
				}
			}
			if(result2 == true)
			{
				// select the Folder
				Log.message("Select Folder is Successfully");
				SkySiteUtils.waitTill(5000);				
				driver.findElement(By.xpath("//*[@id='popup']/div/div/div[3]/div[2]/input[1]")).click();// click on OK Button
				Log.message("Ok button has been clicked");
				SkySiteUtils.waitTill(3000);
				driver.switchTo().defaultContent();
				SkySiteUtils.waitForElement(driver, notytext, 60);
				String Notify_msg = notytext.getText();
				Log.message("Notify Message is:" + Notify_msg);
				if (Notify_msg.contentEquals("File(s) successfully copied"))
				{
					result3 = true;
					Log.message("Files Copied Successfully!!!");
					SkySiteUtils.waitTill(3000);
				}
				else
				{
					result3 = false;
					Log.message("Files Copied Failed!!!");
				}
			}
			else 
			{
				result2 = false;
				Log.message("Select Folder is Failed!!!");
			}
		} 
		else 
		{
			result1 = false;
			Log.message("Select Project Failed!!!");
		}
		// Final Condition
		if((result1 == true)&&(result2 == true)&&(result3 == true)) 		
			return true;		
		else 		
			return false;		
	}

	 @FindBy(css = "#liDocumentHistory1>a")
	 WebElement liDocumentHistory1;
	 
	 @FindBy(xpath = "//i[@class='icon icon-download icon-orange']")
	 WebElement btndownloadiconorange1;
	       
		 
	 /** 
     * Method written for File history download from file more option.
     * Scripted By: Sekhar     
     * @throws AWTException 
     */
   	
    public boolean Filehistory_Download_fromfile_moreoption(String DownloadPath) throws AWTException
    {
    	SkySiteUtils.waitTill(2000);      	
       	// Calling delete files from download folder script
       	fnaHomePage = new FnaHomePage(driver).get();
       	fnaHomePage.Delete_Files_From_Folder(DownloadPath);
       	SkySiteUtils.waitTill(3000);       	
       	driver.switchTo().frame(driver.findElement(By.id("myFrame")));  
    	String FileName= txtfilename.getText();
       	Log.message("File name is:"+FileName);
       	SkySiteUtils.waitTill(3000);       	
       	chkAllProjectdocs.click();
       	Log.message("all check box button has been clicked");
   		SkySiteUtils.waitTill(3000); 
   		btnButton1.click();//click on more
		Log.message("more button has been clicked");
		SkySiteUtils.waitTill(3000);
		liDocumentHistory1.click();
		Log.message("File histroy button has been clicked");
		SkySiteUtils.waitTill(5000);
		btndownloadiconorange1.click();
		Log.message("Download icon button has been clicked");
		SkySiteUtils.waitTill(5000);
   		// Get Browser name on run-time.
   		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
   		String browserName = caps.getBrowserName();
   		Log.message("Browser name on run-time is: " + browserName);
   		
   		if (browserName.contains("firefox"))
   		{
   			// Handling Download PopUp of firefox browser using robot
   			Robot robot = null;
   			robot = new Robot();   			
   			robot.keyPress(KeyEvent.VK_ALT);
   			SkySiteUtils.waitTill(2000);
   			robot.keyPress(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(2000);
   			robot.keyRelease(KeyEvent.VK_ALT);
   			robot.keyRelease(KeyEvent.VK_S);
   			SkySiteUtils.waitTill(3000);
   			robot.keyPress(KeyEvent.VK_ENTER);
   			robot.keyRelease(KeyEvent.VK_ENTER);
   			SkySiteUtils.waitTill(20000);
   		}
   		SkySiteUtils.waitTill(10000);
   		// After checking whether download folder or not
   		String ActualFilename = null;
   		
   		File[] files = new File(DownloadPath).listFiles();

   		for (File file : files) 
   		{
   			if (file.isFile())
   			{
   				ActualFilename = file.getName();// Getting Folder Name into a variable
   				Log.message("Actual File name is:" + ActualFilename);
   				SkySiteUtils.waitTill(1000);
   				Long ActualFileSize = file.length();
   				Log.message("Actual File size is:" + ActualFileSize);
   				SkySiteUtils.waitTill(3000);
   				if (ActualFileSize != 0)
   				{
   					Log.message("Downloaded file is available in downloads!!!");
   				}
   				else
   				{
   					Log.message("Downloaded file is NOT available in downloads!!!");
   				}
   			}
   		}
   		if (ActualFilename.contentEquals(FileName))
   			return true;
   		else
   			return false;   		
    }
    
    @FindBy(css = ".icon.icon-view-files.ico-lg.icon-blue")
	 WebElement btnlgiconblue1;
    
    @FindBy(xpath = ".//*[@id='imageViewer']/div[2]/div[1]/div/div[2]/img[1]")
	 WebElement btnimageViewer;
	       
		 
	 /** 
    * Method written for File history View from file more option.
    * Scripted By: Sekhar     
    * @throws AWTException 
    */
  	
   public boolean Filehistory_View_fromfile_moreoption() throws AWTException
   {
	   SkySiteUtils.waitTill(2000);             	
	   driver.switchTo().frame(driver.findElement(By.id("myFrame")));  
	   String FileName= txtfilename.getText();
      	Log.message("File name is:"+FileName);
      	SkySiteUtils.waitTill(3000);       	
      	chkAllProjectdocs.click();
      	Log.message("all check box button has been clicked");
  		SkySiteUtils.waitTill(3000); 
  		btnButton1.click();//click on more
		Log.message("more button has been clicked");
		SkySiteUtils.waitTill(3000);
		liDocumentHistory1.click();
		Log.message("File histroy button has been clicked");
		SkySiteUtils.waitTill(5000);
		btnlgiconblue1.click();
		Log.message("View icon button has been clicked");
		SkySiteUtils.waitTill(5000);  		
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for (String winHandle : driver.getWindowHandles()) 
		{
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}
		SkySiteUtils.waitTill(8000);
		String ActFilename = driver.findElement(By.xpath("//span[@data-placement='bottom'] [@data-toggle='tooltip']")).getText();
		Log.message("File Name is:"+ActFilename);
  		if((btnimageViewer.isDisplayed()))
  			return true;
  		else
  			return false;   		
   }
    
    
}